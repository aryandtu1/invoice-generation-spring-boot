# Automatic Invoice Generation
### Spring Boot App



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

* Keep your ```maven``` installed
* Run Command: mvn spring-boot:run

## Docker
* Build: ```mvn clean install```
* Building Docker Image: ```docker build -t invoice-generator .```
* Running Docker Application: ```docker run -it --name invoice-container -p 8080:8080 invoice-generator```
* Logs: ```docker logs invoice-container | less```

## Docker Compose 
* Build: ```mvn clean install```
* Start: ```docker compose up -d```
* Stop: ``` docker compose down```


## Dynamic Scheduling Pattern 
```
┌───────────── second (0-59)
│ ┌───────────── minute (0 - 59)
│ │ ┌───────────── hour (0 - 23)
│ │ │ ┌───────────── day of the month (1 - 31)
│ │ │ │ ┌───────────── month (1 - 12) (or JAN-DEC)
│ │ │ │ │ ┌───────────── day of the week (0 - 7)
│ │ │ │ │ │          (or MON-SUN -- 0 or 7 is Sunday)
│ │ │ │ │ │
 * * * * * *
```

#### Some rules apply:

* A field may be an asterisk (*), which always stands for “first-last”. For the day-of-the-month or day-of-the-week fields, a question mark (?) may be used instead of an asterisk.
* Commas (,) are used to separate items of a list.
* Two numbers separated with a hyphen (-) express a range of numbers. The specified range is inclusive.
* Following a range (or *) with / specifies the interval of the number’s value through the range.
* English names can also be used for the day-of-month and day-of-week fields. Use the first three letters of the particular day or month (case does not matter).

Here are some examples:

Cron Expression	          | Meaning

------
* 0 0 * * * *    |   top of every hour of every day

* */10 * * * * *      | every ten seconds

* 0 0 8-10 * * *              | 8, 9 and 10 o’clock of every day

* 0 0 6,19 * * *           | 6:00 AM and 7:00 PM every day

* 0 0/30 8-10 * * *         | 8:00, 8:30, 9:00, 9:30, 10:00 and 10:30 every day

* 0 0 9-17 * * MON-FRI        | on the hour nine-to-five weekdays

* 0 0 0 25 12 ?    | every Christmas Day at midnight