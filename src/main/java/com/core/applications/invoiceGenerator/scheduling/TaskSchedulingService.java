package com.core.applications.invoiceGenerator.scheduling;


import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ScheduledFuture;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TaskSchedulingService {

  @Autowired
  private TaskScheduler taskScheduler;

  Map<String, ScheduledFuture<?>> jobsMap = new HashMap<>();

  public void scheduleATask(String jobId, Runnable tasklet, String cronExpression) {
    log.info(
        "Scheduling task with job id: " + jobId + " and cron expression: " + cronExpression);
    ScheduledFuture<?> scheduledTask = taskScheduler.schedule(tasklet,
        new CronTrigger(cronExpression, TimeZone.getTimeZone(TimeZone.getDefault().getID())));
    jobsMap.put(jobId, scheduledTask);
  }

  public void removeScheduledTask(String jobId) {
    ScheduledFuture<?> scheduledTask = jobsMap.get(jobId);
    if (scheduledTask != null) {
      scheduledTask.cancel(true);
      jobsMap.put(jobId, null);
    }
  }
}