package com.core.applications.invoiceGenerator.scheduling;


import com.core.applications.invoiceGenerator.service.TestService;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Builder
@Data
public class TaskDefinitionBean implements Runnable {

  private TaskDefinition taskDefinition;
  private TestService testService;

  @Override
  public void run() {
    log.info("[Running Task: {}, {}]", taskDefinition, Thread.currentThread().getId());
    testService.invoke(taskDefinition);
  }


}