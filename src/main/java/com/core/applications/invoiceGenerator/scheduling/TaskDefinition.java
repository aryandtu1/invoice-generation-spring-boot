package com.core.applications.invoiceGenerator.scheduling;

import lombok.Data;

@Data
public class TaskDefinition {

  private String invoice_to;
  private String customer_id;
  private String invoice_cron_expression;
  private String invoice_scheduler_id;
}
