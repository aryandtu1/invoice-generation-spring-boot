package com.core.applications.invoiceGenerator.service;

import com.core.applications.invoiceGenerator.scheduling.TaskDefinition;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TestService {

  public void invoke(TaskDefinition taskDefinition) {
    log.info("Invoking Service with data: {}", taskDefinition);
  }
}
