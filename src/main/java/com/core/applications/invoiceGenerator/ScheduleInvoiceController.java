package com.core.applications.invoiceGenerator;


import com.core.applications.invoiceGenerator.scheduling.TaskDefinition;
import com.core.applications.invoiceGenerator.scheduling.TaskDefinitionBean;
import com.core.applications.invoiceGenerator.scheduling.TaskSchedulingService;
import com.core.applications.invoiceGenerator.service.TestService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
public class ScheduleInvoiceController {

  @Autowired
  private TaskSchedulingService taskSchedulingService;

  @Autowired
  private TestService testService;

  @PostMapping(path = "/register_automated_invoice", consumes = "application/json", produces = "application/json")
  public void scheduleATask(@RequestBody TaskDefinition taskDefinition) {
    log.info("Registering Automated Invoice");
    TaskDefinitionBean taskDefinitionBean = TaskDefinitionBean.builder()
        .taskDefinition(taskDefinition).testService(testService).build();
    taskDefinitionBean.setTaskDefinition(taskDefinition);
    taskSchedulingService.scheduleATask(taskDefinition.getInvoice_scheduler_id(),
        taskDefinitionBean,
        taskDefinition.getInvoice_cron_expression());
  }

  @GetMapping(path = "/unregister_automated_invoice/{invoice_scheduler_id}")
  public void removeJob(@PathVariable String invoice_scheduler_id) {
    log.info("Unregistering Automated Invoice for : {}", invoice_scheduler_id);
    taskSchedulingService.removeScheduledTask(invoice_scheduler_id);
  }
}
